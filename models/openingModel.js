const mongoose = require('mongoose')

const OpeningModel = mongoose.model(
    "anime-api",
    {
        title:{
            type: String,
            
        },
        anime:{
            type: String,
        },
        src:{
            type: String,
            
        }, 
        oe:{
            type: Number
        }
    },
    
    "opening"
);

module.exports = {OpeningModel};