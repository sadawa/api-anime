const express = require("express");
const router = express.Router();
const {OpeningModel} = require("../models/openingModel");
const ObjectId = require("mongoose").Types.ObjectId

// On prend tous les data qu'il a dans api
router.get('/', (req,res) => {
    OpeningModel.find((err,docs) => {
        if(!err) res.send(docs);
        else console.lof("Error to get data :" + err)
    })
});
// On rajoute une date supplementaire
router.post('/',(req,res) => {
    const newOped = new OpeningModel({
        title: req.body.title,
        anime: req.body.anime,
        src: req.body.src,
        oe: req.body.oe
    
    });
    newOped.save((err,docs) => {
        if(!err) res.send(docs);
        else console.log("Error creating new data : " + err)
    })
})
// On modifie la data en fonction de id 
router.put("/:id",(req,res)=> {
    if(!ObjectId.isValid(req.params.id))
    return res.status(400).send("ID unknow :" + req.params.id)

    const updateOpening = {
        title: req.body.title,
        anime: req.body.anime,
        src: req.body.src,
        oe: req.body.oe
    
    };

    OpeningModel.findByIdAndUpdate(
        req.params.id,
         {$set : updateOpening},
         {new: true},
            (err,docs) => {
                if(!err) res.send(docs);
                else console.log("Update error : " + err)
            }
    )
})
// On supprime la data de api 
router.delete("/:id",(req,res)=> {
    if(!ObjectId.isValid(req.params.id))
    return res.status(400).send("ID unknow :" + req.params.id)


    OpeningModel.findByIdAndRemove(
        req.params.id,
        (err,docs) => {
            if(!err)
            res.send(docs);
            else console.log("Delete error : " + err)
        })

});


module.exports = router