const bodyParser = require("body-parser");
const express = require("express")
const app = express();
require('./models/dbConfig')
const openingRoute = require("./routes/openingRoute");
const mongoose = require("mongoose");
const cors = require("cors");


app.use(cors());
mongoose.set("useFindAndModify",false);

app.use(bodyParser.json());
app.use("/opening",openingRoute)

app.listen(5000, () => console.log("Server started: 5000"))